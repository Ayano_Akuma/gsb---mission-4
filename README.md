# GSB - Mission 4 #

Le laboratoire GSB est le fruit de la fusion entre Galaxy et le Conglomérat Swiss Bourdin. Il souhaite renouveler l’activité de visite médicale. Les visiteurs médicaux ont la charge de démarcher le personnel. On souhaite rapprocher la gestion du suivi des visites. Les déplacements et actions de terrain génèrent des frais pris en charge par la comptabilité.
 
L’application frais GSB doit être capable de clôturer les fiches d’un visiteur d’une campagne précédente au début d’une campagne. De plus l’application devra mettre en paiement les fiches du mois précédent au 20ème jour du mois. L’application devra être réaliser en C# avec VS.net.
 
Pour la réalisation de cette application, les outils suivants ont été nécessaires : VisualStudio, WampServer, phpMyAdmin et les langages C# et SQL
