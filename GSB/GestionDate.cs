﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GSB
{
    class GestionDate
    {
        private DateTime localDate = DateTime.Now;

        public string MoisActuel()
        {
            string dateComplete = localDate.ToString(); // Format : 19/06/2015 10:35:50, Local
            // Découpage du mois et de l'année
            dateComplete = dateComplete.Substring(3, 7); // Format : 06/2015
            string dateConvertie = dateComplete.Substring(3) + dateComplete.Substring(0, 2); // Format 201506

            return dateConvertie;
        }

        public string MoisPrecedent()
        {
            string dateConvertie;

            if(localDate.Month-1 < 10)
                dateConvertie = localDate.Year.ToString() + '0' + ((localDate.Month)-1).ToString();
            else
                dateConvertie = localDate.Year.ToString() + ((localDate.Month) - 1).ToString();

            return dateConvertie;
        }

        public bool JourEntreUnEtDix()
        {
            if (localDate.Day > 1 && localDate.Day < 10)
                return true;
            else
                return false;
        }

        public bool JourApresVingt()
        {
            if (localDate.Day > 20)
                return true;
            else
                return false;
        }
    }
}
