﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Timers;

namespace GSB
{
    public partial class Form1 : Form
    {
        List<string> fichesMoisPrecedent;
        GestionDate date = new GestionDate();
        ConnexionSql connexion = ConnexionSql.GetInstance("127.0.0.1", "gsb_project", "root", "root");
        NotifyIcon notifyIcon = new NotifyIcon();

        public Form1()
        {
            InitializeComponent();
            listFichesMoisPrecedent.DataSource = fichesMoisPrecedent;
            this.SizeChanged += new EventHandler(Form1SizeChanged);
            notifyIcon.DoubleClick += new EventHandler(NotifyIconMouseDoubleClick);
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            System.Timers.Timer timer = new System.Timers.Timer(1000);// se déclenche toutes les secondes
            timer.Enabled = true;
            timer.AutoReset = true;
            timer.Elapsed += MiseAJourFiche;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            connexion.ConnexionDB(); // On se connecte à la DB
            fichesMoisPrecedent = connexion.ListeFichesMoisPrecedent(date.MoisPrecedent()); // On récupère les fiches du mois précédent

            listFichesMoisPrecedent.DataSource = null; // Force la mise à jour de la listBox
            listFichesMoisPrecedent.DataSource = fichesMoisPrecedent;

            connexion.DeconnexionDB(); // On se déconnecte de la DB
        }

        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }

        private void MiseAJourFiche(Object source, ElapsedEventArgs e)
        {
            if(date.JourEntreUnEtDix())
            {
                connexion.ConnexionDB();
                connexion.Cloture(date.MoisPrecedent());
                connexion.DeconnexionDB();
            }
            else if (date.JourApresVingt())
            {
                connexion.ConnexionDB();
                connexion.Rembourse(date.MoisPrecedent());
                connexion.DeconnexionDB();
            }
        }

        private void Form1SizeChanged(Object source, EventArgs e)
        {
            bool MouseNotOnTaskBar = Screen.GetWorkingArea(this).Contains(Cursor.Position);

            if(this.WindowState == FormWindowState.Minimized && MouseNotOnTaskBar)
            {
                notifyIcon.Icon = SystemIcons.Application;
                this.ShowInTaskbar = false;
                notifyIcon.Visible = true;

            }
        }

        private void NotifyIconMouseDoubleClick(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Normal;
            if (this.WindowState == FormWindowState.Normal)
            {
                this.ShowInTaskbar = true;
                notifyIcon.Visible = false;
            }
        }
    }
}
