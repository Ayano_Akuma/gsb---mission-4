﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace GSB
{
    class ConnexionSql
    {
        //public string serveur, bdd, utilisateur, mdp;

        private static ConnexionSql instance = null;
        private MySqlConnection connection;

        private ConnexionSql(string serveur, string bdd, string utilisateur, string mdp)
        {
            // Préparation de la connexion à la base de données
            string connectionString = "SERVER=" + serveur + ";" + "DATABASE=" + bdd + ";" + "UID=" + utilisateur + ";" + "PASSWORD=" + mdp + ";";
            
            try
            {
                connection = new MySqlConnection(connectionString);    
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public void ConnexionDB()
        {
            try
            {
                // Connexion à la base de données
                connection.Open();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

        }

        public void DeconnexionDB()
        {
            try
            {
                // Déconnexion de la base de données
                connection.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public void Cloture(string moisPrecedent)
        {
            string sql = "UPDATE fichefrais SET id_Etat='CL' WHERE mois="+moisPrecedent+";";

            try
            {
                MySqlCommand executeQuery = new MySqlCommand(sql, connection);
                executeQuery.ExecuteNonQuery();
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public void Rembourse(string moisPrecedent)
        {
            string sql = "UPDATE fichefrais SET id_Etat='RB' WHERE mois=" + moisPrecedent + ";";

            try
            {
                MySqlCommand executeQuery = new MySqlCommand(sql, connection);
                executeQuery.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public List<string> ListeFichesMoisPrecedent(string moisPrecedent)
        {
            List<string> listeFiches = new List<string>();
            string sql = "SELECT * FROM fichefrais WHERE mois=" + moisPrecedent + ";";

            MySqlCommand executeQuery = new MySqlCommand(sql, connection);
            var lignesSql = executeQuery.ExecuteReader();

            while(lignesSql.Read()) // Avance ligne par ligne
            {
                listeFiches.Add(lignesSql.GetString(0) // Récupère la colonne 0 de la ligne
                                + " " + lignesSql.GetString(1)
                                + " " + lignesSql.GetString(2)
                                + " " + lignesSql.GetString(3)
                                + " " + lignesSql.GetString(4)
                                + " " + lignesSql.GetString(5)
                                + " " + lignesSql.GetString(6));
            }

            return listeFiches;
        }

        public static ConnexionSql GetInstance(string serveur, string bdd, string utilisateur, string mdp)
        {
            try
            {
                if (instance == null)
                {
                    instance = new ConnexionSql(serveur, bdd, utilisateur, mdp);
                }
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            return instance;
        }
    }
}
